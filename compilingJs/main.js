$(document).ready(function () {
  $('#slider-inst').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    adaptiveHeight: true,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    } // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });
});

function td() {
  console.log("test");
}

document.querySelector(".in-every-instagram").onclick = td;
gsap.from(".menu__item", {
  opacity: 0,
  duration: 1,
  y: 30,
  stagger: 0.2
});
gsap.from(".hand>img", {
  opacity: 0,
  duration: 1,
  y: 70,
  duration: 1
});
gsap.from(".first-block .title", {
  opacity: 0,
  duration: 1,
  y: 30,
  delay: 0.5
});
gsap.from(".first-block .subtitle", {
  opacity: 0,
  duration: 1,
  y: 30,
  delay: 0.8
});
gsap.from(".first-block .btn__get-doc", {
  opacity: 0,
  duration: 1,
  y: 30,
  delay: 1
});
const controller = new ScrollMagic.Controller(); // concept

const tmConcept = new TimelineMax();
const tween = new TweenMax.from(".concept .title", 0.5, {
  y: 100,
  opacity: 0
});
const tween2 = new TweenMax.from(".concept .concept__body", 0.4, {
  y: 100,
  opacity: 0
});
tmConcept.add(tween);
tmConcept.add(tween2);
const sceneConcept = new ScrollMagic.Scene({
  triggerElement: "#concept",
  reverse: false
}).setTween(tmConcept).addTo(controller); // .addIndicators()
// concept marker

const tmConceptMarker = new TimelineMax();
const markerConcept = new TweenMax.from(".concept .marker-block", 2, {
  x: -500,
  width: 0
});
tmConceptMarker.add(markerConcept);
const sceneConceptMarker = new ScrollMagic.Scene({
  triggerElement: "#concept",
  duration: 1000
}).setTween(tmConceptMarker).addTo(controller); // .addIndicators()
// in-every-instagram

const tmInst = new TimelineMax();
const tweenInstTitle = new TweenMax.from(".in-every-instagram .title", 0.5, {
  y: 100,
  opacity: 0
});
const tweenInstBigImg = new TweenMax.from(".in-every-instagram .insta-red-bg__white-heart-center", 0.7, {
  x: -400,
  opacity: 0
});
tmInst.add(tweenInstTitle);
tmInst.add(tweenInstBigImg);
const sceneInst = new ScrollMagic.Scene({
  triggerElement: "#in-every-instagram",
  reverse: false
}).setTween(tmInst).addTo(controller); // .addIndicators()
// advantages

const tmAdvantages = new TimelineMax();
const AdvantagesInstTitle = new TweenMax.from(".advantages .title", 0.5, {
  y: 100,
  opacity: 0
});
const AdvantagesItems = new TweenMax.from(".advantages .advantages__item", 0.5, {
  scale: 0.6,
  opacity: 0,
  stagger: 0.2
});
tmAdvantages.add(AdvantagesInstTitle);
tmAdvantages.add(AdvantagesItems);
const sceneAdvantages = new ScrollMagic.Scene({
  triggerElement: "#advantages",
  reverse: false
}).setTween(tmAdvantages).addTo(controller); // .addIndicators()
// best-time

const bestTime = new TimelineMax();
const bestTimeTitle = new TweenMax.from(".best-time .title", 0.5, {
  y: 100,
  opacity: 0
});
bestTime.add(bestTimeTitle);
const sceneBestTime = new ScrollMagic.Scene({
  triggerElement: "#best-time",
  reverse: false
}).setTween(bestTime).addTo(controller); // .addIndicators()
// trademark

const trademark = new TimelineMax();
const trademarkTitle = new TweenMax.from("#trademark .title", 0.5, {
  y: 100,
  opacity: 0
});
bestTime.add(trademarkTitle);
const sceneTrademark = new ScrollMagic.Scene({
  triggerElement: "#trademark",
  reverse: false
}).setTween(trademark).addTo(controller); // .addIndicators()
// five-steps

const fiveSteps = new TimelineMax();
const fiveStepsTitle = new TweenMax.from("#five-steps .title", 0.5, {
  y: 100,
  opacity: 0
});
const fiveStep = new TweenMax.from("#five-steps .five-step__step", 0.4, {
  y: 100,
  scale: 0.6,
  opacity: 0,
  stagger: 0.2
});
fiveSteps.add(fiveStepsTitle);
fiveSteps.add(fiveStep);
const sceneFiveSteps = new ScrollMagic.Scene({
  triggerElement: "#five-steps",
  reverse: false
}).setTween(fiveSteps).addTo(controller); // .addIndicators()

const markers = document.querySelectorAll(".marker");
document.addEventListener("mousemove", e => {
  markers.forEach(marker => {
    const speed = marker.dataset.speed;
    const x = (window.innerWidth - e.pageX * speed) / 120;
    const y = (window.innerWidth - e.pageY * speed) / 120;
    marker.style.setProperty('transform', `translate(${x}px, ${y}px)`);
  });
});