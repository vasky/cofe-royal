const burgerIcon = document.querySelector(".mobile-burger__icon")
const menu = document.querySelector(".menu")
const xIcon = document.querySelector(".xIcon")

burgerIcon.onclick = ()=>{
    toggleMenu()
}
xIcon.onclick = () => {
    toggleMenu()
}

function toggleMenu(){
    if(menu.style.display === 'flex'){
        closeMenu()
    }else{
        showMenu()
    }
}

function showMenu(){
    menu.style.display = "flex"
}
function closeMenu(){
    menu.style.display = "none"
}
