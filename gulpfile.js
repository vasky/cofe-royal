'use strict';

var sass = require('gulp-sass');
const gulp = require('gulp')
const concat = require('gulp-concat')
const babel = require("gulp-babel")
const { dest } = gulp

gulp.task('sass', function() {
    return gulp.src('./sass/*.sass') // Gets all files ending with .scss in app/scss and children dirs
        .pipe(concat('all.sass'))
        .pipe(sass())
        .pipe(gulp.dest('css/'))
})

gulp.task('js', function(){
    return gulp.src("./js/*.js")
        .pipe(babel())
        .pipe(dest('compilingJs/'))
        .pipe(concat('all.js'))
        .pipe(babel())
        .pipe(dest('compilingJs/'))
})

// gulp.watch('/scss/*.scss', ['sass']);
gulp.task('watch', function(){

    gulp.watch('./sass/*.sass', gulp.series('sass'));
    gulp.watch('./js/*.js', gulp.series('js'));
    // Other watchers
})
